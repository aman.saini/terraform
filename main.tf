terraform {
  backend "http"{
  }

}


variable "gitlab_access_token" {
  type = string
}



# Configure the GitLab Provider
provider "gitlab" {
  token = var.gitlab_access_token
}

data "gitlab_project" "example_project"{

    id = "36631586"

}


# Add a variable to the project
resource "gitlab_project_variable" "sample_project_variable" {
  project = gitlab_project.sample_project.id
  key     = "project_variable_key"
  value   = "project_variable_value"
}



